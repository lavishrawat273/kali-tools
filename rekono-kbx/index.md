---
Title: rekono-kbx
Homepage: https://github.com/pablosnt/rekono
Repository: https://gitlab.com/kalilinux/packages/rekono-kbx
Architectures: amd64
Version: 1.6.3
Metapackages: 
Icon: images/rekono-logo.svg
PackagesInfo: |
 ### rekono-kbx
 
  Rekono is an automation platform that combines different hacking tools to
  complete pentesting processes.
   
  Rekono combines other hacking tools and its results to execute complete
  pentesting processes against a target in an automated way. The findings
  obtained during the executions will be sent to the user via email or
  Telegram notifications and also can be imported in Defect-Dojo if an
  advanced vulnerability management is needed. Moreover, Rekono includes a
  Telegram bot that can be used to perform executions easily from anywhere
  and using any device.
 
 **Installed size:** `133 KB`  
 **How to install:** `sudo apt install rekono-kbx`  
 
 {{< spoiler "Dependencies:" >}}
 * docker.io | docker-ce
 * kaboxer 
 {{< /spoiler >}}
 
 ##### rekono
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
